let number = Number(prompt("Enter number here"));
console.log("The number you provided is " + number + ".")

for(let input = number; input != 0; input--){
    if(input <= 50){
        console.log("The current value is at 50. Terminating the loop.");
        console.log(input);
        break;
    }
    if(input%10==0){
        console.log("The number is divisible by 10. Skipping the number.")
        continue;
    }
    if(input%5==0){
        console.log(input);
    }
}

let longWord = "supercalifragilisticexpialidocious";
let consonantLetters = "";
for(let i=0; i<longWord.length;i++){
    if(
        longWord[i].toLowerCase()=='a' ||
        longWord[i].toLowerCase()=='e' ||
        longWord[i].toLowerCase()=='i' ||
        longWord[i].toLowerCase()=='o' ||
        longWord[i].toLowerCase()=='u'){
        continue;
    }
    else{
        consonantLetters+=longWord[i];
    }

}

console.log(longWord);
console.log(consonantLetters);

